import pymongo
from pymongo import MongoClient
import datetime
import dateutil

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')

db = client.LK_monitor # use database myDB
coll = db.Monitor

from_date = datetime.datetime(2023, 10, 10, 15, 00, 0, 000000)
to_date = datetime.datetime(2023, 10, 11, 15, 00, 0, 000000)

a = coll.remove(
  {  
    'date': 
    { 
        "$gte": from_date, 
        "$lt": to_date
    }
  }
)
print(a)

last_N = 10
# query last_N data from collection
data_row = coll.find().skip(coll.count() - last_N)
for x in data_row:
  print x

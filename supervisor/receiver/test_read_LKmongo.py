import pymongo
from pymongo import MongoClient
import datetime

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')

db = client.LK_monitor # use database myDB
coll = db.Monitor


# print all data in collection
# data_row = coll.find_one()
# for data_row in coll.find():
#   print(data_row)

last_N = 10

# query last_N data from collection
data_row = coll.find().skip(coll.count() - last_N)
for x in data_row:
  print x

# query all Kband data from collection print last_N data
data_row = coll.find({ "sensorName": "Kband"})
print data_row.count()
data_row = data_row.skip(data_row.count() - last_N)
for x in data_row:
  print x

# query all Lband data from collection print last_N data
data_row = coll.find({ "sensorName": "Lband"})
print data_row.count()
data_row = data_row.skip(data_row.count() - last_N)
for x in data_row:
  print x

__author__ = "Teep Chairin"
__copyright__ = "Copyright 2020, The visailar mornitor"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "-"
__email__ = "teep@narit.or.th"
__status__ = "Test"

import requests
import serial
from time import time, sleep

url = 'https://notify-api.line.me/api/notify'
token = 'PT6ljx9jkxOYhZzCtYjCIxUnSmN3OqQc14RtfmwEviC'
headers = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token
           }

flagcheck = True
minute = 0

tempArr  = [20,20,21,21,22,23,23,23,23,24,25,26,27,28,28,29,29,30,30,30,31,32,30,29,28,27,26,25,24,23,22,21,20]
humidArr = [81,80,75,70,70,65,60,60,60,60,60,60,50,50,45,45,40,40,40,40,40,40,40,45,50,50,60,60,65,65,70,80,81]

period = 10

while True:
    # do every 1 minuite
    sleep(period - time() % period)

    print(minute)

    if(minute == len(humidArr)):
        minute = 0
        t = 0

    temp = tempArr[minute]
    humid = humidArr[minute]

    # check condition if system normal
    if(flagcheck == False and (temp < 28 and humid < 75)):
        msg = "[SYSTEM INFO]:The system's back to normal."
        r = requests.post(url, headers=headers , data = {'message':msg})
        print(msg)
        print(r.text)
        flagcheck = True
        
    # if system error notify every minute
    if(flagcheck == False):
        msg = "[SYSTEM ERROR] temp:"+ str(temp)+" humid:"+ str(humid)
        r = requests.post(url, headers=headers , data = {'message':msg})
        print(msg)
        print(r.text)
        
    # check condition of temp and humid
    if(flagcheck == True and (temp >= 30 or humid >= 80)):
        msg = "[SYSTEM ERROR]:The system has a malfunction." + "temp:" + str(temp)+" humid:"+ str(humid)
        r = requests.post(url, headers=headers , data = {'message':msg})
        print(msg)
        print(r.text)
        flagcheck = False    

    # send every 1 Hr.
    if(minute%60 == 0 and flagcheck == True):
        msg = "[SYSTEM LOG]:temp " + str(temp)+" humid:"+ str(humid)
        r = requests.post(url, headers=headers , data = {'message':msg})
        print(msg)
        print(r.text)

    minute += 1

    # add this script to supervisor



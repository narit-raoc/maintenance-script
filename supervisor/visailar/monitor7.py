__author__ = "Teep Chairin"
__copyright__ = "Copyright 2020, The visailar mornitor"
__license__ = "GPL"
__version__ = "4.1"
__maintainer__ = "-"
__email__ = "teep@narit.or.th"
__status__ = "Test"

"""
history
- version 1
- version 2
- version 3
- version 4
    - version 4.1
        - send alarm to only "Backend_alarm" line group
- version 5
    - add mongodb database

- version 6 None
    - 31/10/22 : bug monitor after mongodb stop and can't write sensor to  
    - 01/11/22 : add msg for send error to line too
    - 13/01/23 : change ttyUSB0 to ttyUSB1
- version 7 
    - 09/08/23 : fixing the data temperature and humidity not change
                - change interval read to 2 minuite
                - edite state add more connect and disconnect state
                - change ttyUSB1 to ttyUSB0
"""

import sys
import requests
import serial
from time import time, sleep
import datetime
import threading

import pymongo
from pymongo import MongoClient


url = 'https://notify-api.line.me/api/notify'
token = 'PT6ljx9jkxOYhZzCtYjCIxUnSmN3OqQc14RtfmwEviC'
token_Backend_alarm = 'OqMzFbzExLRSbIYoLsd1zvS4SrFwsUtsmZV18ZdBUDW'

headers = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token
           }

headers_Backend_alarm = {
            'content-type':
            'application/x-www-form-urlencoded',
            'Authorization':'Bearer '+token_Backend_alarm
           }

slack_webhook_url = "https://hooks.slack.com/services/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"

def send_to_slack(message):
    alert_message = {
        "text": message
    }
    try:
        response = requests.post(slack_webhook_url, json=alert_message)  
        if response.status_code == 200:
            return("Alert sent to Slack!")
        else:
            return("Failed to send alert:" + {response.text})
    except Exception as e:
        print("ERROR, send_to_slack :" + str(e))    

# global variable
# testing = True
testing = False
ser = None

flagcheck = True
flagsend = True
flagdataisnull =  True
count = 0
secound = 60*2
temp = 0
humid = 0

msg_error = ""

delta = datetime.timedelta(hours=1)
now = datetime.datetime.now()
next_hour = (now + delta).replace(microsecond=0, second=0, minute=2)
wait_seconds = (next_hour - now).seconds

# Mongo Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.visalar_monitor # use database myDB
coll = db.Monitor

temperatureStats = {
        "sensorName": "Visalar",
        "Temp": None,
        "Humid": None,
        "date": datetime.datetime.now()
    }

msg = "start service Visalar monitoring: " + str(now) + " and will start the loop in next 1 h"
print(msg)



if(testing != True):
    send_to_Backend_alarm_thatProgramStar = requests.post(url, headers=headers , data = {'message':msg})
    send_to_Backend_alarm_thatProgramStar = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
    send_to_slack(msg)

    sleep(wait_seconds) # wait for next hour notification to line application

now = datetime.datetime.now()
print("start loop service in next hour " + str(now))

def send_every_2min():
    global flagsend
    global temp
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*2)
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            sleep(1)
            if(flagdataisnull == False):
                try:
                    msg = "[Visalar SYSTEM LOG from send_every_2min]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 2 min")
                    state = 0
                except Exception as e:
                    print("ERROR, send every 2 min :" + str(e))

def send_every_1hour():
    global flagsend
    global temp
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*60*1)  
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[Visalar SYSTEM LOG from send_every_1hour]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 1 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 1 hr :" + str(e))    
        
def send_every_6hour():
    global flagsend
    global temp 
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*60*6)
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[Visalar SYSTEM LOG from send_every_6hour]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 6 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 6 hr :" + str(e))    
    
def send_every_12hour():
    global flagsend
    global temp 
    global humid
    state = 0
    while True:
        if(state == 0):
            sleep(60*60*12)    
            state = 1
        if(state == 1):
            # do case 1 in loop when flagdataisnull is True
            # if send requests.post done do next state again
            if(flagdataisnull == False):            
                try:
                    msg = "[Visalar SYSTEM LOG from send_every_12hour]:temp " + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    print("DONE, send noti every 12 hr")
                    flagsend = False
                    state = 0      
                except Exception as e:
                    print("ERROR, send every 12 hr :" + str(e))     
        
def read_and_check():
    global count
    global temp
    global humid
    global flagdataisnull
    global flagcheck
    global msg_error
    global ser
    '''
    state info:
    -1 = error state
    10 = connect to Vaisala 
    20 = read data from Vaisala every 1 minuite
    30 = check condition (normal, alert 1 minuite if abnormal, abnormal)
    40 = write databese to mongo
    50 = disconneted from Vaisala 
    '''
    state = 10
    while True:
        if(state == -1):
            # error state
            msg = "[Visalar SYSTEM ERROR Visalar]: " + msg_error
            r = requests.post(url, headers=headers , data = {'message':msg})
            send_to_slack(msg)
            ser_bytes = ""
            humid = 0.0
            temp = 0.0
            flagdataisnull = True
            state = 10
        
        if(state ==10):
            try:
                ser = serial.Serial(port = "/dev/ttyUSB0", baudrate=9600,bytesize=8, timeout=5, stopbits=serial.STOPBITS_ONE)
                ser.flushInput()
                state = 20
            except Exception as e:
                print("can't connect to Vaisala " + str(e))
                msg_error = "state: " + str(state) + " msg: " + str(e)
                sleep(5)
                state = -1

        if(state == 20):
            # do every 2 minuite
            sleep(secound - time() % secound)
            # clear data 
            humid = 0.0
            temp = 0.0
            ser_bytes = ""
            # read visailar
            try:
                ser_bytes = ser.readline()
                # print(str(ser_bytes))
                # print(str(ser_bytes[0]))
                # sub string 
                if(ser_bytes != "" and ser_bytes[0] == "R"):
                    humid = ser_bytes[4:8]
                    temp = ser_bytes[16:20]
                    humid = float(humid)
                    temp = float(temp)
                    print("temp : " + str(temp) + " humid : " + str(humid))
                    flagdataisnull = False
                    state = 30
                else:
                    flagdataisnull = True
                    print("ser_bytes = null")
                    state = 20
            except Exception as e:
                print("serail error:" + str(e))
                msg_error = "state: " + str(state) + " msg: " + str(e)
                state = -1

        if(state == 30): 
            # check condition if system normal
            try:      
                if(flagcheck == False and (temp < 28 and humid < 75)):
                    msg = "[Visalar SYSTEM INFO]:The system's back to normal."
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    flagcheck = True
            except Exception as e:
                print("check normal error:" + str(e))
                msg_error =  "state: " + str(state) + " msg: " + str(e)
                state = -1

            # if system error let send alram every minute
            try:
                if(flagcheck == False):
                    msg = "[Visalar SYSTEM ERROR] temp:"+ str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_Backend_alarm = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)        
            except Exception as e:
                print("if Visalar SYSTEM error notify every minute error:" + str(e))   
                msg_error =  "state: " + str(state) + " msg: " + str(e)
                state = -1

            # check condition of temp and humid are error?
            try:
                if(flagcheck == True and (temp >= 27 or humid >= 80)):
                    msg = "[Visalar SYSTEM ERROR]:The system has a malfunction." + "temp:" + str(temp)+" humid:"+ str(humid)
                    r = requests.post(url, headers=headers , data = {'message':msg})
                    send_to_Backend_alarm = requests.post(url, headers=headers_Backend_alarm , data = {'message':msg})
                    send_to_slack(msg)
                    print(msg)
                    print(r.text)
                    flagcheck = False            
            except Exception as e:
                print("check abnormal error:" + str(e)) 
                msg_error =  "state: " + str(state) + " msg: " + str(e)
                state = -1

            state = 40
        
        if(state == 40):
            temperatureStats = {
                "sensorName": "Visalar",
                "Temp": int(temp),
                "Humid": int(humid),
                "date": datetime.datetime.now()
                }

            try:
                coll.insert_one(temperatureStats)
                # print("write to mongo done")                    
            except Exception as error:
                print("write_sensor_to_mongo " + error)
                msg_error = "can't write mongo" + "msg: " + str(error)
                state = -1

            state = 50
        
        if(state == 50):
            try:
                ser.flushInput()    # clear serial port
                ser.close()                 
            except Exception as error:
                print("can't disconnect vaisala " + error)
                msg_error = "can't disconnect vaisala" + "msg: " + str(error)
                state = -1

            state = 10
     
if __name__ == '__main__':
    read_and_checkThread = threading.Thread(target=read_and_check)
    read_and_checkThread.daemon = True
    read_and_checkThread.start()

    global testing


    if(testing == True):
        SendEvery2MinThread = threading.Thread(target=send_every_2min)
        SendEvery2MinThread.daemon = True
        SendEvery2MinThread.start()

    # SendEvery1HourThread = threading.Thread(target=send_every_1hour)
    # SendEvery1HourThread.daemon = True
    # SendEvery1HourThread.start()

    # SendEvery6HourThread = threading.Thread(target=send_every_6hour)
    # SendEvery6HourThread.daemon = True
    # SendEvery6HourThread.start()

    SendEvery12HourThread = threading.Thread(target=send_every_12hour)
    SendEvery12HourThread.daemon = True
    SendEvery12HourThread.start()

    while True:
        try:
            sleep(2)
        except KeyboardInterrupt:
            print("kill")
            # timerThread.join(0.1)       
            read_and_checkThread.join(0.1)
            SendEvery12HourThread.join(0.1)
            if(testing == True):
                SendEvery2MinThread.join(0.1)
            
            ser.flushInput()    # clear serial port
            ser.close()
            sys.exit()




# add this script to supervisor
# add grehp

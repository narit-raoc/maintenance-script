import requests

slack_webhook_url = "https://hooks.slack.com/services/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"
alert_message = {
    "text": "🚨 *ALERT!* CPU usage is over 90%! 🚨"
}

response = requests.post(slack_webhook_url, json=alert_message)

if response.status_code == 200:
    print("Alert sent to Slack!")
else:
    print(f"Failed to send alert: {response.text}")
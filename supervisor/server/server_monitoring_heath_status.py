__author__ = "Teep Chairin"
__copyright__ = "-"
__license__ = "-"
__version__ = "-"
__maintainer__ = "-"
__email__ = "teep@narit.or.th"
__status__ = "Test"


"""
history
- 24.01.23 first create

"""
import sys
import requests
from time import time, sleep
import datetime
import threading

import pandas # sudo apt install python3-pandas, sudo pip3 install pandas
import os

import pymongo
from pymongo import MongoClient

"""
check list
1. check 8 node server
    - nvidia-smi
    - mount mnt/tnrt/
    - superviosord

2. check storage server
    - capasity avilible
    - error on web gui
    - temperature and humudity

3. check beegfs
    - server must running for all server

4. check ntp server
    - check all server using ntp server

"""

import ansible_runner

runner_result = ansible_runner.run(private_data_dir='/home/teep/tnrt-server-ansible/playbooks/', inventory=my_inventory, playbook='ping.yaml')
print(runner_result.stats)
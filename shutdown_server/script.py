#!/usr/bin/env python3

import os
from fabric import Connection
from invoke import Responder
from dotenv import dotenv_values

config = dotenv_values(".env")

SERVERS = [
    {
        'ip': '192.168.90.2',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    },
    {
        'ip': '192.168.90.3',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    },
    {
        'ip': '192.168.90.4',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    },
    {
        'ip': '192.168.90.5',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    },
    {
        'ip': '192.168.90.6',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    },
    {
        'ip': '192.168.90.7',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    },
    {
        'ip': '192.168.90.8',
        'user': config['SERVER_8_NODE_USER'],
        'password': '{}\n'.format(config['SERVER_8_NODE_PASS'])
    }
]

for x in SERVERS:
    connectionString = '{}@{}:22'.format(x['user'], x['ip'])
    print(connectionString)
    server = Connection(connectionString)
    sudopass = Responder(pattern=r'\[sudo\] password', response=x['password'])
    try:
        server.run('sudo shutdown -h now', pty=True, watchers=[sudopass])
    except Exception as e:
        print(e)

os.system("shutdown -h now")
